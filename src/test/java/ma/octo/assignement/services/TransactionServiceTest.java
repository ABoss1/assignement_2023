package ma.octo.assignement.services;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.*;
import ma.octo.assignement.service.ITransactionService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

import static ma.octo.assignement.mapper.TransactionMapperTest.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class TransactionServiceTest {
    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private DepositRepository depositRepository;

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Test
    void saveTransactionThrowsExceptionWhenMontantPassedLimitPerDay() throws TransactionException, CompteNonExistantException {

    }
    @Test
    void saveTransactionThrowsExceptionWhenMontantIsNullOrLessThanMin() {
        Deposit deposit = getDepositInstance();
        deposit.setMontant(BigDecimal.valueOf(0));
        Exception exception = assertThrows(TransactionException.class,()->transactionService.saveTransaction(deposit));
        assertEquals("Montant minimal de transfer non atteint",exception.getMessage());
    }

    @Test
    void saveTransactionThrowsExceptionWhenMontantPassedLimit() {

        Deposit deposit = getDepositInstance();
        deposit.setMontant(BigDecimal.valueOf(20000));
        Exception exception  = assertThrows(TransactionException.class,()-> transactionService.saveTransaction(deposit));
        assertEquals(exception.getMessage(),"Montant maximal de transfer dépassé");

    }
}
