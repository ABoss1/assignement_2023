package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.implementations.TransactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import java.math.BigDecimal;
import java.util.Date;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;



@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class TransactionMapperTest {


    @Autowired
    @Qualifier(value = "depositMapper")
    private ITransactionMapper depositMapper;

    @Autowired
    private CompteRepository compteRepository;

    @InjectMocks
    TransactionService transactionService;

    private static final Date now = new Date();

    public static Utilisateur getUserInstance(){
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");
        utilisateur1.setActive(true);
        utilisateur1.setBirthdate(new Date());
        utilisateur1.setPassword("root");
        utilisateur1.setRole("ADMIN");
        return utilisateur1;
    }

    public static Compte getCompteInstance(){
        Compte compte = new Compte();
        compte.setNrCompte("account nbr1");
        compte.setSolde(BigDecimal.valueOf(10000.0));
        compte.setRib("rib1");
        compte.setUtilisateur(getUserInstance());
        return compte;
    }
    public static Compte getCompteInstance1(){
        Compte compte = new Compte();
        compte.setNrCompte("account nbr2");
        compte.setSolde(BigDecimal.valueOf(10000.0));
        compte.setRib("rib2");
        compte.setUtilisateur(getUserInstance());
        return compte;
    }
    public static DepositDto getDepositDtoInstance(){
        DepositDto depositDto = new DepositDto();
        depositDto.setNom_prenom_emetteur("ahmed boussouir");
        depositDto.setMontant(BigDecimal.TEN);
        depositDto.setDate(now);
        depositDto.setNrCompteBeneficiaire("account nbr1");
        depositDto.setMotif("motif 1");
        return depositDto;
    }

    public static Deposit getDepositInstance(){
        Deposit deposit = new Deposit();
        deposit.setNomPrenomEmetteur("ahmed boussouir");
        deposit.setMontant(BigDecimal.TEN);
        deposit.setMotif("motif 1");
        deposit.setDateExecution(now);
        deposit.setCompteBeneficiaire(getCompteInstance());
        return deposit;
    }
    public static Transfer getTransferInstance(){
        Transfer transfer = new Transfer();
        transfer.setMotif("motif 2");
        transfer.setMontant(BigDecimal.valueOf(5000));
        transfer.setDateExecution(now);
        transfer.setCompteBeneficiaire(getCompteInstance());
        transfer.setCompteEmetteur(getCompteInstance1());
        return transfer;
    }
    @Test
    void mappsDomainToDto() {

        Deposit deposit = getDepositInstance();
        DepositDto result = (DepositDto) depositMapper.domainToDto(deposit);

        assertEquals("ahmed boussouir", result.getNom_prenom_emetteur());
        assertEquals(BigDecimal.TEN, result.getMontant());
        assertEquals("motif 1", result.getMotif());
        assertEquals("account nbr1", result.getNrCompteBeneficiaire());
        assertEquals(now, result.getDate());

    }

    @Test
    void mappsDtoToDomain() throws CompteNonExistantException {
        DepositDto depositDto = getDepositDtoInstance();
        compteRepository.save(getCompteInstance());
        Deposit result = (Deposit) depositMapper.dtoToDomain(depositDto);

        assertEquals("ahmed boussouir", result.getNomPrenomEmetteur());
        assertEquals(BigDecimal.TEN, result.getMontant());
        assertEquals("motif 1", result.getMotif());
        assertEquals("account nbr1", result.getCompteBeneficiaire().getNrCompte());
        assertEquals(now, result.getDateExecution());

    }






}
