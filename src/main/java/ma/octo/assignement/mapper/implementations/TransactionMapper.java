package ma.octo.assignement.mapper.implementations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class TransactionMapper  {

    @Autowired
    private CompteRepository compteRepository;


    public TransactionDto domainToDto(Transaction transaction) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setDate(transaction.getDateExecution());
        transactionDto.setNrCompteBeneficiaire(transaction.getCompteBeneficiaire().getNrCompte());
        transactionDto.setMontant(transaction.getMontant());
        transactionDto.setMotif(transaction.getMotif());

        return transactionDto;
    }


    public  Transaction dtoToDomain(TransactionDto transactionDto) throws CompteNonExistantException {
        Compte recepteur = compteRepository.findByNrCompte(transactionDto.getNrCompteBeneficiaire());
        if ( recepteur == null){
            throw new CompteNonExistantException("Compte non existant");
        }
        Transaction transaction = new Transaction();
        transaction.setCompteBeneficiaire(recepteur);
        transaction.setMontant(transactionDto.getMontant());
        transaction.setDateExecution(transactionDto.getDate());
        transaction.setMotif(transactionDto.getMotif());

        return transaction;
    }
}
