package ma.octo.assignement.mapper.implementations;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.ITransactionMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class DepositMapper extends TransactionMapper implements ITransactionMapper {

    @Override
    public TransactionDto domainToDto(Transaction transaction) {

        DepositDto depositDto = new DepositDto();
        BeanUtils.copyProperties(super.domainToDto(transaction),depositDto);

        depositDto.setNom_prenom_emetteur(((Deposit)transaction).getNomPrenomEmetteur());
        return depositDto;
    }

    @Override
    public Transaction dtoToDomain(TransactionDto transactionDto) throws CompteNonExistantException {
        Deposit deposit = new Deposit();
        BeanUtils.copyProperties(super.dtoToDomain(transactionDto),deposit);

        deposit.setNomPrenomEmetteur(((DepositDto)transactionDto).getNom_prenom_emetteur());
        return deposit;
    }
}
