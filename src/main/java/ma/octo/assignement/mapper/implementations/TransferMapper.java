package ma.octo.assignement.mapper.implementations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.mapper.ITransactionMapper;
import ma.octo.assignement.repository.CompteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransferMapper extends TransactionMapper implements ITransactionMapper {

    @Autowired
    private CompteRepository compteRepository;

    @Override
    public Transaction dtoToDomain(TransactionDto transactionDto) throws CompteNonExistantException {
        Transfer transfer = new Transfer();
        BeanUtils.copyProperties(super.dtoToDomain(transactionDto),transfer);
        Compte emetteur = compteRepository.findByNrCompte(((TransferDto)transactionDto).getNrCompteEmetteur());

        if (emetteur == null ){
            throw new CompteNonExistantException("Compte non existant");
        }
        transfer.setCompteEmetteur(emetteur);

        return transfer;
    }

    @Override
    public TransactionDto domainToDto(Transaction transaction){
        TransferDto transferDto = new TransferDto();
        BeanUtils.copyProperties(super.domainToDto(transaction),transferDto);
        transferDto.setNrCompteEmetteur(((Transfer)transaction).getCompteEmetteur().getNrCompte());

        return transferDto;
    }


}
