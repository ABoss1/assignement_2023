package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransactionDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;

public interface ITransactionMapper {
    TransactionDto domainToDto(Transaction transaction);
    Transaction dtoToDomain(TransactionDto transactionDto) throws CompteNonExistantException;


}
