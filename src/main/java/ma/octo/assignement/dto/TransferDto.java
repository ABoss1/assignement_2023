package ma.octo.assignement.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class TransferDto extends TransactionDto {
  private String nrCompteEmetteur;

}
