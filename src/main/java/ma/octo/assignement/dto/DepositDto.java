package ma.octo.assignement.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class DepositDto extends TransactionDto {
    private String nom_prenom_emetteur;
}
