package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface ITransactionService {
    void saveTransaction(Transaction transaction) throws CompteNonExistantException, TransactionException;
}
