package ma.octo.assignement.service;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface IDepositService {
    List<Deposit> getAll();
    void saveDeposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;
}
