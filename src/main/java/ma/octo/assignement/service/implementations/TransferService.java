package ma.octo.assignement.service.implementations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.ITransactionMapper;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.ITransactionService;
import ma.octo.assignement.service.ITransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class TransferService implements ITransferService {
    public static final int MONTANT_MAXIMAL = 10000;


    @Autowired
    @Qualifier(value = "transferMapper")
    private ITransactionMapper transferMapper;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private IAuditService audit;

    @Override
    public List<Transfer> getAll() {
        return transferRepository.findAll();
    }

    @Override
    public void saveTransfer(TransferDto transferDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Transfer transfer = (Transfer) transferMapper.dtoToDomain(transferDto);
        Compte emetteur = transfer.getCompteEmetteur();
        BigDecimal montant = transfer.getMontant();

        if (emetteur.getSolde().compareTo(montant) < 0){
            throw new SoldeDisponibleInsuffisantException("solde insuffisant pour effectuer l'operation");
        }

        transactionService.saveTransaction(transfer);

        emetteur.setSolde(emetteur.getSolde().subtract(montant));
        compteRepository.save(emetteur);

        transferRepository.save(transfer);

        audit.auditTransfer(transfer, EventType.TRANSFER);
    }


}
