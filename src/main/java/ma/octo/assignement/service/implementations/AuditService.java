package ma.octo.assignement.service.implementations;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditTransferRepository;
import ma.octo.assignement.service.IAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService implements IAuditService {

    @Autowired
    private AuditTransferRepository auditTransferRepository;

    public void auditTransfer(Transaction transaction, EventType eventType) {

        StringBuilder message = new StringBuilder();
        message.append("Transfer depuis ");

        if (eventType.equals(EventType.TRANSFER)){
            message.append(((Transfer)transaction).getCompteEmetteur().getNrCompte());
        }else{
            message.append(((Deposit)transaction).getNomPrenomEmetteur());
        }

        message.append(" vers ").append(transaction.getCompteBeneficiaire().getNrCompte())
                .append(" d'un montant de ").append(transaction.getMontant());

        Audit audit = new Audit();
        audit.setEventType(eventType);
        audit.setMessage(message.toString());
        auditTransferRepository.save(audit);
    }



}
