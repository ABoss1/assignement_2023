package ma.octo.assignement.service.implementations;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.ITransactionMapper;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IDepositService;
import ma.octo.assignement.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class DepositService implements IDepositService {

    @Autowired
    private DepositRepository depositRepository;

    @Autowired
    private ITransactionService transactionService;

    @Autowired
    @Qualifier(value = "depositMapper")
    private ITransactionMapper depositMapper;

    @Autowired
    private IAuditService audit;


    @Override
    public List<Deposit> getAll() {
        return depositRepository.findAll();
    }

    @Override
    public void saveDeposit(DepositDto depositDto) throws CompteNonExistantException, TransactionException {
        //System.out.println("deposit service");
        Deposit deposit = (Deposit) depositMapper.dtoToDomain(depositDto);

        transactionService.saveTransaction(deposit);

        depositRepository.save(deposit);
        audit.auditTransfer(deposit,EventType.DEPOSIT);
    }
}
