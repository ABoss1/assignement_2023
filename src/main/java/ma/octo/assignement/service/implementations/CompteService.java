package ma.octo.assignement.service.implementations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.ICompteService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompteService implements ICompteService {
    private CompteRepository compteRepository;

    @Override
    public List<Compte> getAll() {
        return compteRepository.findAll();
    }
}
