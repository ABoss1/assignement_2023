package ma.octo.assignement.service.implementations;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransactionRepository;
import ma.octo.assignement.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class TransactionService implements ITransactionService {
    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;
    public static final int MONTANT_MAX_PER_DAY = 20000;

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public void saveTransaction(Transaction transaction) throws TransactionException, CompteNonExistantException {

        Compte recepteur = transaction.getCompteBeneficiaire();
        BigDecimal montant = transaction.getMontant();
        if (recepteur == null){
            throw new CompteNonExistantException("compte inexisant");
        }
        if (montant.intValue() < MONTANT_MINIMAL) {
            throw new TransactionException("Montant minimal de transfer non atteint");
        }
        if (montant.intValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Montant maximal de transfer dépassé");
        }
        if (transaction.getMotif() == null) {
            throw new TransactionException("Motif vide");
        }

        Date today = new Date();
        today.setHours(0);
        today.setMinutes(0);
        today.setSeconds(0);

        List<Transaction> transactionList = transactionRepository.findTransactionsByCompteBeneficiaireAndAndDateExecutionAfter(recepteur,today);

        BigDecimal sum = transactionList.stream().map(Transaction::getMontant).reduce(BigDecimal.ZERO,BigDecimal::add);
        if (transaction.getMontant().add(sum).intValue() >  MONTANT_MAX_PER_DAY){
            throw new TransactionException("Montant maximal de transfer par jour dépassé");
        }

        recepteur.setSolde(recepteur.getSolde().add(montant));
        compteRepository.save(recepteur);
    }
}
