package ma.octo.assignement.service;

import ma.octo.assignement.domain.Transaction;
import ma.octo.assignement.domain.util.EventType;

public interface IAuditService {
    void auditTransfer(Transaction transaction, EventType eventType);

}
