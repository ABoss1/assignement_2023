package ma.octo.assignement.domain;

import lombok.*;

import javax.persistence.*;


@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Deposit extends Transaction{

  @Column
  private String nomPrenomEmetteur;


}
