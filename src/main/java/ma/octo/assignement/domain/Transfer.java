package ma.octo.assignement.domain;

import lombok.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Transfer extends Transaction{

  @ManyToOne(cascade = {CascadeType.ALL})
  private Compte compteEmetteur;

}
