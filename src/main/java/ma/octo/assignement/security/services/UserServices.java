package ma.octo.assignement.security.services;


import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServices implements IUserServices{

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UtilisateurRepository userRepository;
    @Override
    public Utilisateur encryptPassword(Utilisateur user) {
        String encryptedPass = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPass);
        return user;
    }
    public Utilisateur saveUser(Utilisateur user){
        return userRepository.save(encryptPassword(user));
    }

    @Override
    public Utilisateur findByUsername(String username) {
        return userRepository.findByUsername(username).get();
    }

}
