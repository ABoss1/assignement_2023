package ma.octo.assignement.security.services;



import ma.octo.assignement.domain.Utilisateur;

import javax.servlet.http.HttpServletRequest;

public interface ITokensServices {
    String getToken(Utilisateur user, int mins, boolean withclaims) throws Exception;
    String refreshToken(HttpServletRequest request) throws Exception;
}
