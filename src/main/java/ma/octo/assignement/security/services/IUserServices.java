package ma.octo.assignement.security.services;


import ma.octo.assignement.domain.Utilisateur;


public interface IUserServices {
    Utilisateur encryptPassword(Utilisateur user);
    Utilisateur saveUser(Utilisateur user);
    Utilisateur findByUsername(String username);
}
