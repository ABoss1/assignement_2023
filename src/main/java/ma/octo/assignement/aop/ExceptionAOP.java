package ma.octo.assignement.aop;


import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Aspect
@Configuration
public class ExceptionAOP {
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(ExceptionAOP.class));

    @Pointcut("execution(* ma.octo.assignement.service.implementations.TransactionService.*(..))")
    public void TransactionExceptionPointCut(){}

    @AfterThrowing(pointcut = "TransactionExceptionPointCut()",throwing = "e")
    public void logTransactionException(Exception e){
        LOGGER.info(e.getMessage());
    }

    @Pointcut("execution(* ma.octo.assignement.service.implementations.TransferService.*(..))")
    public void TransferExceptionPointCut(){}

    @AfterThrowing(pointcut = "TransferExceptionPointCut()",throwing = "e")
    public void logTransferExcption(Exception e) {
        LOGGER.info(e.getMessage());
    }

    @Pointcut("execution(* ma.octo.assignement.mapper.ITransactionMapper.*(..))")
    public void TransactionMappingExceptionPointCut(){}

    @AfterThrowing(pointcut = "TransactionMappingExceptionPointCut()",throwing = "e")
    public void logTransactionMappingExcption(Exception e) {
        LOGGER.info(e.getMessage());
    }
}
