package ma.octo.assignement.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Logger;

@Aspect
@Configuration
public class AuditAOP {
    private static final Logger LOGGER = Logger.getLogger(String.valueOf(AuditAOP.class));

    @Pointcut("execution(* ma.octo.assignement.service.implementations.AuditService.*(..))")
    public void AuditServicePointcut(){}

    @AfterReturning("AuditServicePointcut()")
    public void logAudit(JoinPoint joinPoint){
        String message = joinPoint.toLongString();
        LOGGER.info("Audit de l'événement {}"+ joinPoint.getArgs()[1] + ", details : "+ message);
    }
}
