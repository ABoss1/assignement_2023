package ma.octo.assignement.web;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IDepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(value = "/deposits")
public class DepositController {

    @Autowired
    private IDepositService depositService;

    @GetMapping("/")
    public List<Deposit> getAll(){
        return depositService.getAll();
    }

    @PostMapping("/save")
    public void saveDeposit(@RequestBody DepositDto depositDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        depositService.saveDeposit(depositDto);
    }
}
