package ma.octo.assignement.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.security.services.ITokensServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class SecurityController {

    @Autowired
    ITokensServices tokensServices;

    @PostMapping("/authenticate")
    public void createAuthToken(@RequestBody Utilisateur user , HttpServletResponse response) throws Exception{

        String refresh_token = "Bearer" + tokensServices.getToken(user,1,false);
        final Cookie cookie = new Cookie("refresh_token", refresh_token );
        cookie.setSecure(true);
        cookie.setHttpOnly(true);
        cookie.setMaxAge((int) (System.currentTimeMillis() + 1000L *60*5));
        //response.setHeader("Access-Control-Allow-Origin","http://localhost:3000");

        String access_token = tokensServices.getToken(user,2,true);

        Map<String, String> tokens = new HashMap<>();
        tokens.put("access_token",access_token);

        response.addCookie(cookie);
        response.setContentType(APPLICATION_JSON_VALUE);
        new ObjectMapper().writeValue(response.getOutputStream(),tokens);
    }

    @GetMapping("/refresh_token")
    public void refreshToken(HttpServletResponse response, HttpServletRequest request) throws Exception {
        Map<String, String> tokens = new HashMap<>();
        Map<String, String> error = new HashMap<>();
        String access_token = tokensServices.refreshToken(request);
        //response.setHeader("Access-Control-Allow-Origin","http://localhost:3000");

        if (access_token!=null){
            tokens.put("access_token",access_token);
            response.setContentType(APPLICATION_JSON_VALUE);
            new ObjectMapper().writeValue(response.getOutputStream(),tokens);
        }else{
            response.setStatus(FORBIDDEN.value());
            error.put("error_code", "1");
            response.setContentType(APPLICATION_JSON_VALUE);
            new ObjectMapper().writeValue(response.getOutputStream(),error);
        }

    }


}
